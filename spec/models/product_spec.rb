require 'rails_helper'

RSpec.describe Product, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:reviews).dependent(:destroy) }
  end
end

# == Schema Information
#
# Table name: products
#
#  id          :integer          not null, primary key
#  original_id :integer
#  name        :string
#  price       :float
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  url         :text
#
