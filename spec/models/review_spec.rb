require 'rails_helper'

RSpec.describe Review, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:product) }
  end

  describe '.filter' do
    let(:product) { create(:product) }
    let!(:review_1) { create(:review, product: product, text: 'text') }
    let!(:review_2) { create(:review, product: product, text: 'uniq text') }

    context 'without term' do
      it 'should return all result' do
        reviews = Review.filter(nil)
        expect(reviews.size).to eq 2
        expect(reviews).to eq Review.all
      end
    end

    context 'with term' do
      it 'should filter by text' do
        reviews = Review.filter('uniq')
        expect(reviews.size).to eq 1
        expect(reviews.first).to eq review_2
      end
    end
  end
end

# == Schema Information
#
# Table name: reviews
#
#  id         :integer          not null, primary key
#  product_id :integer
#  title      :string
#  text       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
