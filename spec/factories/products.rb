FactoryGirl.define do
  factory :product do
    original_id { Faker::Number.number 3 }
    name        { Faker::Name.name }
    price       { Faker::Number.decimal 2 }
    url         { Faker::Internet.url }
  end
end

# == Schema Information
#
# Table name: products
#
#  id          :integer          not null, primary key
#  original_id :integer
#  name        :string
#  price       :float
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  url         :text
#
