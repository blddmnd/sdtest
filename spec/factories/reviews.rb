FactoryGirl.define do
  factory :review do
    product
    title { Faker::Name.title }
    text  { Faker::Lorem.sentence }
  end
end

# == Schema Information
#
# Table name: reviews
#
#  id         :integer          not null, primary key
#  product_id :integer
#  title      :string
#  text       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
