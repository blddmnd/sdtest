require 'rails_helper'

RSpec.describe ProductsController, type: :controller do
  describe 'GET #index' do
    let!(:product) { create(:product) }

    before { get :index }

    it 'should be success' do
      expect(response).to be_success
    end

    it 'assings list of products' do
      expect(assigns(:products)).to eq Product.all
    end
  end

  describe 'GET #show' do
    let(:product) { create(:product) }
    let!(:review) { create(:review, product: product, text: 'review text') }

    it 'should be success' do
      get :show, params: { id: product }
      expect(response).to be_success
    end

    it 'should assigns product' do
      get :show, params: { id: product }
      expect(assigns(:product)).to eq product
    end

    it 'should filter reviews' do
      get :show, params: { id: product, term: 'text' }
      expect(assigns(:reviews).size).to eq 1
      expect(assigns(:reviews).first).to eq review

      get :show, params: { id: product, term: 'no_results' }
      expect(assigns(:reviews).size).to eq 0
    end
  end

  describe 'POST #create' do
    it 'should call parser class' do
      allow(WalmartParser).to receive(:call).and_return(
        InteractorStub.new(options: { product: create(:product) })
      )

      expect(WalmartParser).to receive(:call).with(url: 'http://google.com')
      post :create, params: { url: 'http://google.com' }
      expect(response).to have_http_status(302)
    end
  end
end
