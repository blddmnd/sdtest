Please:

- create a small web application that allows to input URL of walmart product. Example:

http://www.walmart.com/ip/Ematic-9-Dual-Screen-Portable-DVD-Player-with-Dual-DVD-Players-ED929D/28806789

- program should save to database: name of the product and it's price

- program should than save into database ALL the reviews for a given product

- we don't care about the user interface

- additionally it should also allow to filter the reviews by a given keyword

Questions to ask yourself:

1) how do you assure that when you rerun the program for the same product there no duplicates are created? (either in products or reviews)

2) Why did you choose a given technology? Do you know any better options? What are the limitations?

3) What kind of choices did you make or avoided? Patterns? Best Practices? Document any side effects and possible problems when scaling up the application (can be in points we will talk about that during our interview).

We don't want you to lose too much time so ~3 hours should be enough, if you don't finish just publish what you have.
If you have questions you can ask me or you can just take some decision yourself treat is as an open task.
