require 'open-uri'

class ReviewParser
  include Interactor

  attr_reader :page, :product

  before do
    @product = context[:product]
    @page = context[:page]
  end

  def call
    # TODO Need to parse reviews from other pages
    page.css('[itemprop="review"]').each do |review_block|
      title = review_block.at_css('[itemprop="name"]').text
      text = review_block.at_css('.review-body').text

      Review.create(
        product: product,
        title: title,
        text: text
      )
    end
  rescue => e
    Rails.logger.error(e)
  end

  private

  # TODO Need to create module with these two methods and use it here and in PageParser
  def get_page url
    Nokogiri::HTML(get(url))
  end

  def get url
    open(url)
  end
end
