require 'open-uri'

class PageParser
  include Interactor

  attr_reader :url, :page

  before { @url = context[:url] }

  def call
    # TODO Need to add additional check for url here. What if url not from walmart or without product id?
    original_id = url.split('/').last

    # skip if we have this record into the database
    if product = Product.find_by(original_id: original_id.to_i)
      context[:product] = product and return
    end

    # parse information from page
    @page = get_page(url)

    name = page.at_css('[itemprop="name"]').children[0].text
    price = page.at_css('[itemprop="price"]').attributes['content'].text

    product = Product.create(
      url: url,
      original_id: original_id,
      name: name,
      price: price
    )

    context[:product] = product
    context[:page] = page
  rescue => e
    Rails.logger.error(e)
    context.fail! error: "Can't parse page from entered URL"
  end

  private

  def get_page url
    Nokogiri::HTML(get(url))
  end

  def get url
    open(url)
  end
end
