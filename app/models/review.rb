class Review < ApplicationRecord
  belongs_to :product

  scope :filter, ->(term) { where('text LIKE ?', "%#{term}%") if term }

  # TODO Need to add some validations here
end

# == Schema Information
#
# Table name: reviews
#
#  id         :integer          not null, primary key
#  product_id :integer
#  title      :string
#  text       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
