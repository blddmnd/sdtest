class Product < ApplicationRecord
  has_many :reviews, dependent: :destroy

  # TODO Need to add some validations here
end

# == Schema Information
#
# Table name: products
#
#  id          :integer          not null, primary key
#  original_id :integer
#  name        :string
#  price       :float
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  url         :text
#
