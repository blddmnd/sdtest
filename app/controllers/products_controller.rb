class ProductsController < ApplicationController
  def index
    @products = Product.all
  end

  def show
    @product = Product.find(params[:id])
    @reviews = @product.reviews.filter(params[:term])
  end

  def create
    result = WalmartParser.call(url: params[:url])

    if result.success?
      redirect_to result.product
    else
      # we can get error here and display it #{result.error}
      redirect_to root_path
    end
  end
end
