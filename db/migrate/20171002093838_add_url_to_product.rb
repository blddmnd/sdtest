class AddUrlToProduct < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :url, :text
  end
end
